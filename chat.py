from openai import OpenAI

client = OpenAI()

user_input = input("In cosa posso esserti utile?")

completion = client.chat.completions.create(
    model="gpt-3.5-turbo",

    messages=[
        {"role": "user", "content": user_input}
    ]
)

print(completion.choices[0].message)
