# OpenAI GPT-3 Chatbot Script

This script allows you to interact with the GPT-3 model from OpenAI for generating responses to user input.

## Prerequisites

Before running the script, make sure you have your OpenAI API key exported. You can export it using the following command:

```bash
export OPENAI_API_KEY=<your_api_key_here>
